package com.sothebys.jenkins;

/**
 * Represent a version per semver conventions.
 *
 * @author Gregor Zurowski
 */
class SemVer implements Serializable {

  String version

  /**
   * @return The major version, if <code>version</code> is not parsable, then return <code>1</code>.
   */
  def getMajorVersion() {
    version?.substring(0, version?.indexOf('.')) ?: '1'
  }

}