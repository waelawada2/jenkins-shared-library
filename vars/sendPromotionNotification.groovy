#!groovy

/**
 * sendPromotionNotification
 *
 * Send a notification e-mail about pending promotion steps.
 *
 * @param config.environment  The name of the target deployment environment.
 * @param config.recipients  A comma-separated list of e-mail addresses.
 * @param config.branch  Name of the branch
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def environment = config.environment.toUpperCase()
  def recipients  = config.recipients
  def branch      = config.branch

  // create URL to job pipeline view
  def pipelineUrl = env.BUILD_URL.substring(0, env.BUILD_URL.length() - 1)
  pipelineUrl = pipelineUrl.substring(0, pipelineUrl.lastIndexOf('/'))

  mail subject: "Promotion of job '${env.JOB_NAME}' #${env.BUILD_NUMBER} to ${environment}",
       from: 'jenkins@sothebys.com',
       to: recipients,
       body: """Promote job '${env.JOB_NAME}' #${env.BUILD_NUMBER}${branch != null ? ' (branch \'' + branch + '\') ' : ' '}to ${environment} environment:
- From pipeline view at ${pipelineUrl}
- From console output at ${env.BUILD_URL}console"""

}
