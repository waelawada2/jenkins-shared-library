#!groovy

/**
 * sendBuildNotification
 *
 * Send a build notifications via e-mail.
 *
 * @param config.recipients  A comma-separated list of e-mail addresses.
 * @param config.success  Indicated whether the build was successful.
 * @param config.branch  Name of the branch
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def recipients = config.recipients
  def success    = config.success
  def branch     = config.branch

  def status = success ? 'SUCCESSFUL' : 'FAILED'

  mail subject: "Job '${env.JOB_NAME}' #${env.BUILD_NUMBER} ${status}",
       from: 'jenkins@sothebys.com',
       to: recipients,
       body: """Job '${env.JOB_NAME}' #${env.BUILD_NUMBER}${branch != null ? ' (branch \'' + branch + '\') ' : ' '}${status}:
- Summary available at ${env.BUILD_URL}
- Console output available at ${env.BUILD_URL}console"""

}
