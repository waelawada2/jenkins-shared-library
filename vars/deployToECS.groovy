#!groovy

/**
 * Deploy an application to ECS
 *
 * @param config.application  The name of the application.
 * @param config.registry  The URI of the ECS container registry.
 * @param config.cluster  The name of the target ECS cluster.
 * @param config.environment  The deployment environment.
 * @param config.tag  The tag of the container to be deployed.
 * @param config.port  The port used by the container.
 * @param config.containerName  The name of the container (needs to be aligned with the name in the task definition).
 *
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def application   = config.application
  def majorVersion  = config.majorVersion
  def registry      = config.registry
  def cluster       = config.cluster
  def environment   = config.environment
  def tag           = config.tag
  def port          = config.port
  def containerName = config.containerName

  echo "-------------------------------"
  echo "Deploy ${application}-${tag} to ${environment.toUpperCase()} environment"
  echo "-------------------------------"

  withEnv(["APP_NAME=${application}", "MAJOR_VERSION=${majorVersion ?: ''}", "REGISTRY=${registry}", "CLUSTER=${cluster}", "ENVIRONMENT=${environment}", "TAG=${tag}", "CONTAINER_PORT=${port}", "CONTAINER_NAME=${containerName ?: ''}"]) {
    sh '''
      # Include major version, if provided
      if [ -n "${MAJOR_VERSION}" ]; then
        SERVICE_NAME="${APP_NAME}-v${MAJOR_VERSION}-${ENVIRONMENT}"
        TASK_FAMILY="${APP_NAME}-v${MAJOR_VERSION}-${ENVIRONMENT}"
      else
        SERVICE_NAME="${APP_NAME}-${ENVIRONMENT}"
        TASK_FAMILY="${APP_NAME}-${ENVIRONMENT}"
      fi

     # Use the app name as container name, if none explicitly provided
     if [ -z "${CONTAINER_NAME}" ]; then
       CONTAINER_NAME="${APP_NAME}"
     fi

     # Create new task definition
     sed -e "s;%IMAGE_URI%;${REGISTRY}:${TAG};g" taskdef.${ENVIRONMENT}.ecs.json > taskdef-${TAG}.${ENVIRONMENT}.ecs.json
     aws ecs register-task-definition --family ${TASK_FAMILY} --cli-input-json file://taskdef-${TAG}.${ENVIRONMENT}.ecs.json

     # Create or update service
     SERVICE_EXISTS=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq .failures[]`
     SERVICE_STATUS=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq -r .services[0].status`
     if [ "$SERVICE_EXISTS" == "" ] && [ "${SERVICE_STATUS}" != "INACTIVE" ]; then
       echo "Updating existing service"
       DESIRED_COUNT=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq .services[].desiredCount`
       if [ ${DESIRED_COUNT} = "0" ]; then
         DESIRED_COUNT="1"
       fi
       TASK_REVISION=`aws ecs describe-task-definition --task-definition ${TASK_FAMILY} | jq .taskDefinition.revision`
       aws ecs update-service --service ${SERVICE_NAME} --cluster ${CLUSTER} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count ${DESIRED_COUNT}
     else
       echo "Creating new service"
       TARGET_GROUP_ARN=`aws elbv2 describe-target-groups | jq -r ".TargetGroups[] | select(.TargetGroupName | contains(\\"${SERVICE_NAME}\\")) | .TargetGroupArn"`
       aws ecs create-service --service-name ${SERVICE_NAME} --cluster ${CLUSTER} --task-definition ${TASK_FAMILY} --desired-count 1 --load-balancers "targetGroupArn=${TARGET_GROUP_ARN},containerName=${CONTAINER_NAME},containerPort=${CONTAINER_PORT}" --role ecsServiceRole
     fi
    '''
  }

}
