#!groovy

/**
 * Push a Docker image to the given registry.
 *
 * @param config.application  The name of the application.
 * @param config.tag  The tag of the image.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def application = config.application
  def tag = config.tag

  sh "docker build -t ${application}:${tag} ."

}