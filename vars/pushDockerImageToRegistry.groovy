#!groovy

/**
 * Push a Docker image to the given registry.
 *
 * @param config.application  The name of the application.
 * @param config.tag  The tag of the image.
 * @param config.uri  The URI of the Docker container registry.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def application = config.application
  def tag = config.tag
  def uri = config.uri

  echo "-------------------------------"
  echo "Push ${application}-${tag} to registry ${uri}"
  echo "-------------------------------"

  withEnv(["APPLICATION=${application}", "URI=${uri}", "TAG=${tag}"]) {
    sh '''
       docker tag ${APPLICATION}:${TAG} ${URI}:${TAG}
       $(aws ecr get-login --region us-east-1 --no-include-email)
       docker push ${URI}:${TAG}
       '''
  }

}
